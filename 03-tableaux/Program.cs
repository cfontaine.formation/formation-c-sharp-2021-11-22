﻿using System;

namespace _03_tableaux
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Tableau à une dimension
            //int[] tab = null;
            //tab = new int[10];
            int[] tab = new int[10];

            // Valeur d'initialisation par défaut des éléments du tableau
            // entier -> 0
            // nombre flottant ->0.0
            // char -> '\u0000'
            // bool -> false
            // référence -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[2]);
            tab[3] = 123;
            Console.WriteLine(tab[3]);

            // Propriété Length -> Nombre d'élément du tableau
            Console.WriteLine(tab.Length);

            // Déclaration et initialisation
            string[] tStr = { "azerty", "bonjour", "hello", "world" };

            // Parcourir complétement un tableau
            // avec une boucle for
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine(tab[i]);
            }

            // avec une boucle foreach
            foreach (var s in tStr) // string
            {
                Console.WriteLine(s);
                // s = "a"; // erreur: s est accessible uniquement en lecture
            }

            // si l'on essaye d'accèder à un élément en dehors du tableau -> exception
            // Console.WriteLine(tStr[40]);

            // La taille du tableau à la déclaration peut être une variable
            int n = 5;
            char[] tChr = new char[n];
            #endregion

            #region Exercice: Tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau
            Console.WriteLine("Entrer le nombre d'élément du tableau");
            int size = Convert.ToInt32(Console.ReadLine()); // 2
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }

            // int[] t = { -7, -4, -8, -9, -3 };  // 1
            int maximum = t[0];
            double somme = 0.0;
            foreach (var v in t)
            {
                if (v > maximum)
                {
                    maximum = v;
                }
                somme += v;
            }
            Console.WriteLine($"Maximum={maximum} Moyenne={somme / t.Length}");

            #region Tableau à 2 dimensions
            double[,] tab2d = new double[3, 2]; // Déclaration d'un tableau à 2 dimensions

            tab2d[0, 1] = 12.3;     // Accès à un élémént d'un tableau à 2 dimensions
            Console.WriteLine(tab2d[0, 0]);

            // Nombre d'élément du tableau
            Console.WriteLine(tab2d.Length); // 6
            // Nombre de dimension du tableau
            Console.WriteLine(tab2d.Rank); // 2
            // Nombre de ligne
            Console.WriteLine(tab2d.GetLength(0)); // 3
            // Nombre de colonne
            Console.WriteLine(tab2d.GetLength(1)); // 2

            // Parcourir complétement un tableau à 2 dimensions
            // boucle for
            for (int l = 0; l < tab2d.GetLength(0); l++)
            {
                for (int c = 0; c < tab2d.GetLength(1); c++)
                {
                    Console.Write($"tab2d[{l},{c}]={tab2d[l, c]}\t");
                }
                Console.WriteLine("");
            }

            // boucle foreach
            foreach (var d in tab2d)
            {
                Console.WriteLine(d);
            }

            // Déclarer un tableau à 2 dimension en l'initialisant
            char[,] t2dChr = { { 'a', 'z', 'e' }, { 'r', 't', 'y' } };
            foreach (var c in t2dChr)
            {
                Console.WriteLine(c);
            }

            // Tableau 3D
            // Déclaration d'un tableau à 3 dimensions en l'initialisant
            int[,,] tab3d =
            {
                {{2,4,5 }, {2,4,12 },{4,3,1 }},
                {{3,4,5 }, {5,4,9 },{7,4,4 }}
            };

            foreach (var c in tab3d)
            {
                Console.WriteLine(c);
            }
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            int[][] tabEsc = new int[3][];
            tabEsc[0] = new int[3];
            tabEsc[1] = new int[2];
            tabEsc[2] = new int[4];

            // accès à un élément
            tabEsc[1][1] = 2;

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length); // 3

            //Nombre de colonne pour la première ligne
            Console.WriteLine(tabEsc[0].Length); // 3
            Console.WriteLine(tabEsc[1].Length); // 2
            Console.WriteLine(tabEsc[2].Length); // 4
            #endregion

            //  Parcourir complétement un tableau de tableau
            //  boucle for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.WriteLine(tabEsc[i][j]);
                }
            }

            //  boucle foreach
            foreach (int[] row in tabEsc)
            {
                foreach (int v in row)
                {
                    Console.WriteLine(v);
                }
            }
            #endregion

            Console.ReadKey();
        }
    }
}
