﻿using System;

namespace _05_poo
{
    internal class VoiturePrioritaire : Voiture // VoiturePrioritaire hérite de Voiture
    {

        public bool Gyro { get; set; }

        // base => pour appeler le constructeur de la classe mère
        public VoiturePrioritaire() // : base() implicite
        {
            Console.WriteLine("Constructeur par défaut voiture prioritaire");
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma) : base(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 3 paramètres voiture prioritaire");
        }

        public void AlumerGyro()
        {
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        // Redéfinition
        public override void Afficher()
        {
            base.Afficher();
            Console.WriteLine($"Gyro {Gyro}");
        }

        // Occultation => redéfinir une méthode d'une classe mère et à « casser » le lien vers la classe mère
        public new void Accelerer(int vAcc)
        {
            // base => pour appeler une méthode de la classe mère
            base.Accelerer(2 * vAcc);
        }
    }
}
