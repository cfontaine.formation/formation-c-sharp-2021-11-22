﻿using System;

namespace _05_poo
{
    // internal sealed class Voiture
    internal class Voiture
    {
        // Variables d'instances => Etat

        // On n'a plus besoin de déclarer les variables d'instances _marque,_plaqueIma, _compteurKm, elles seront générées automatiquement par les propriétées automatique
        //string marque;
        //string plaqueIma;
        //int compteurKm=200;
        string _couleur = "Rouge";  // Variable d'instance utilisée par la propriété Couleur (C# 7.0)   
        int _vitesse;               // Variable d'instance utilisée par la propriété Vitesse (propriété avec une condition)


        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Marque { get; }
        public string PlaqueIma { get; set; }
        public int CompteurKm { get; private set; } = 200;  // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        // Variable de classe
        // static int compteurVoiture;  // Remplacer par une propriétée static
        public static int CompteurVoiture { get; private set; }   // On peut associer un modificateur d'accès à get et à set. Il doit être plus restritif que le modificateur de la propriété

        // Propriété C#1
        public int Vitesse
        {
            get
            {
                return _vitesse;
            }
            set
            {
                if (value > 0)
                {
                    _vitesse = value;
                }
            }
        }

        // Propriété c#7
        public string Couleur
        {
            get => _couleur;
            set => _couleur = value;
        }

        // Agrégation
        public Personne Proprietaire { get; set; }

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            CompteurVoiture++;
            Console.WriteLine("Constructeur par défaut");
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur 3 paramètres");
            Marque = marque;
            _couleur = couleur;
            PlaqueIma = plaqueIma;
        }

        // Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, int vitesse)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse, int compteurKm) : this(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 5 paramètres");
            _vitesse = vitesse;
            CompteurKm = compteurKm;
        }

        public Voiture(string couleur, string marque, string plaqueIma, Personne proprietaire) : this(couleur, marque, plaqueIma)
        {
            Proprietaire = proprietaire;
        }


        // Destructeur
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur");
        //}

        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur static");
        }

        // Méthode get et set En java et en C++
        //public int getVitesse()
        //{
        //    return vitesse;
        //}

        //public void setVitesse(int vitesse)
        //{
        //    if (vitesse >= 0)
        //    {
        //        this.vitesse = vitesse;
        //    }
        //}



        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                _vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                _vitesse -= vFrn;   // vitesse=vitesse-vFRn
            }
        }

        public void Arreter()
        {
            _vitesse = 0;
        }

        public bool EstArreter()
        {
            return _vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"Voiture[{Marque} {_couleur} {PlaqueIma} {_vitesse} {CompteurKm}]");
            if (Proprietaire != null)
            {
                Console.Write("Propriétaire: ");
                Proprietaire.Afficher();
            }
        }

        public bool EgalVitesse(Voiture vA)
        {
            return _vitesse == vA._vitesse;
        }

        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            Console.WriteLine(CompteurVoiture);
            // vitesse = 0; // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            // Freiner(40);  // ou une méthode d'instance
        }

        public static bool EgaliteVitesse(Voiture vA, Voiture vB)
        {
            return vA._vitesse == vB._vitesse;  // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }

    }
}
