﻿using System;

namespace _05_poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"Variable de classe {Voiture.CompteurVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture("Honda", "Blanc", "Fr-4524-az");
            Console.WriteLine($"Variable de classe {Voiture.CompteurVoiture}");

            // Accès à une variable d'instance
            // v1._vitesse = 10;

            // Accès à une propriété en ecriture (set)
            v1.Vitesse = 10;

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Vitesse);  //v1._vitesse)

            // Appel d’une méthode d’instance
            v1.Accelerer(20);
            v1.Afficher();
            v1.Freiner(10);
            v1.Afficher();
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());

            Voiture v2 = new Voiture();
            // v2._vitesse = 20;
            Console.WriteLine($"Variable de classe {Voiture.CompteurVoiture}");

            // Test de l'appel du destructeur
            v2 = null;  // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                        // Il sera détruit lors de la prochaine execution du garbage collector
                        // Forcer l'appel le garbage collector

            // appel explicite du garbage collector (à éviter)
            //GC.Collect();

            Voiture v3 = new Voiture("ford", "noir", "fr-3554-ER", 40, 10);
            Console.WriteLine($"Variable de classe {Voiture.CompteurVoiture}");

            // appel de méthode de classe
            Console.WriteLine(Voiture.EgaliteVitesse(v1, v3));
            Console.WriteLine(v1.EgalVitesse(v3));
            #endregion

            #region Classe Imbriquée
            Conteneur c = new Conteneur();
            c.Test();
            #endregion

            #region Classe partielle
            Form1 f1 = new Form1(42);
            f1.Afficher();
            #endregion

            #region Classe Statique
            //   Math ma = new Math();
            Console.WriteLine(Math.Abs(-1));

            // Méthode d'extention
            string tstInv = "Bonjour";
            tstInv.Inverser();
            Console.WriteLine(tstInv.Inverser());
            #endregion

            #region Agrégation
            Adresse adr = new Adresse("1, rue Esquermoise", "Lille", "59800");
            Personne per1 = new Personne("John", "Doe", adr);
            v1.Proprietaire = per1;
            v1.Afficher();
            Voiture v4 = new Voiture("Toyota", "jaune", "fr-1342-ER", per1);
            v4.Afficher();
            #endregion

            #region Exercice Compte Bancaire
            CompteBancaire cb = new CompteBancaire(100.0, per1);// "John Doe"
            // cb.solde = 100.0;
            // cb.iban = "fr5962-0000";
            // cb.titulaire = "John Doe";

            cb.Crediter(100.0);
            cb.Afficher();
            cb.Debiter(50.0);
            cb.Afficher();
            Console.WriteLine(cb.EstPositif());

            CompteBancaire cb2 = new CompteBancaire(200.0, new Personne("Jane", "Doe", adr));
            cb2.Afficher();
            #endregion

            #region Exercice Point
            Point p1 = new Point();
            p1.Afficher();
            p1.Deplacer(1, 4);
            p1.Afficher();
            Point p2 = new Point(1, 3);
            p2.Afficher();
            Console.WriteLine($"Norme={p1.Norme()}");
            Console.WriteLine($"Distance={Point.Distance(p1, p2)}");
            #endregion

            #region Indexeur
            Phrase phrase = new Phrase(10);
            phrase.Ajouter("Il");
            phrase.Ajouter("pleut");
            phrase.Ajouter("aujourd'hui");
            phrase.Ajouter("à");
            phrase.Ajouter("Lille");

            Console.WriteLine(phrase[1]);
            phrase[4] = "Nantes";
            Console.WriteLine(phrase[4]);

            Console.WriteLine(phrase[15]);
            // phrase[15] = "Bonjour";

            phrase.Afficher();
            #endregion

            #region Heritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.Accelerer(10);
            vp1.Afficher();
            vp1.AlumerGyro();
            Console.WriteLine(vp1.Gyro);
            vp1.EteindreGyro();
            Console.WriteLine(vp1.Gyro);

            CompteEpargne ce1 = new CompteEpargne(0.75, new Personne("Alan", "Smithee", new Adresse("Boulevard Vincent Gache", "Nantes", "44000")));
            ce1.Crediter(500.0);
            ce1.Afficher();
            ce1.CalculInterets();
            ce1.Afficher();
            #endregion
            Console.ReadKey();

        }
    }
}
