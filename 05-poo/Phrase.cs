﻿using System;

namespace _05_poo
{
    internal class Phrase
    {
        string[] Mots;

        int cptMot;

        public Phrase(int nbMot)
        {
            Mots = new string[nbMot];
        }

        public void Ajouter(string mot)
        {
            if (cptMot < Mots.Length)
            {
                Mots[cptMot] = mot;
                cptMot++;
            }
        }

        // Indexeur
        public string this[int index]
        {
            get
            {
                if (index <= cptMot)
                {
                    return Mots[index];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (index <= cptMot)
                {
                    Mots[index] = value;
                }
                else
                {
                    throw new IndexOutOfRangeException();
                }
            }
        }

        // On peut avoir plusieurs indexeurs dans une classe, il faut que le type et le nombre de paramètre soit diférent (surcharge)
        //public int this[int ligne ,int colonne ]
        //{
        //    get
        //    {
        //        return 0;
        //    }
        //    set
        //    {
        //
        //    }
        //}


        public void Afficher()
        {
            for (int i = 0; i < cptMot; i++)
            {
                Console.Write(Mots[i] + " ");
            }
            Console.WriteLine("\n");
        }
    }
}
