﻿using System;

namespace _05_poo
{
    internal class Personne
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        // Agrégation
        public Adresse Adr { get; set; }

        public Personne(string prenom, string nom)
        {
            Prenom = prenom;
            Nom = nom;
        }

        public Personne(string prenom, string nom, Adresse adr) : this(prenom, nom)
        {
            Adr = adr;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Prenom} {Nom}");
            if(Adr != null)
            {
                Adr.Afficher();
            }
        }
    }
}
