﻿using System;

namespace _05_poo
{
    internal class CompteBancaire
    {
        //  public double solde;
        public double Solde { get; protected set; }
        //public string iban;

        public string Iban { get; private set; }

        //public string titulaire;
        // public string Titulaire { get; set; }
        public Personne Titulaire { get; set; }

        static int cpt;

        public CompteBancaire()
        {
            cpt++;
            Iban = "fr-5962-0000-" + cpt;
        }

        public CompteBancaire(Personne titulaire) : this()
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
            // this.titulaire = titulaire;
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0.0;
        }

        public void Afficher()
        {
            Console.WriteLine("____________________");
            Console.WriteLine($"Solde = {Solde}");
            Console.WriteLine($"Iban = {Iban}");
            if (Titulaire != null)
            {
               Titulaire.Afficher();
            }
            Console.WriteLine("____________________");
        }
    }
}
