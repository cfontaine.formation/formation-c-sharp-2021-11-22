﻿namespace _05_poo
{
    // Classe partielle répartie sur plusieurs fichiers
    internal partial class Form1
    {
        private int data;

        public Form1(int data)
        {
            this.data = data;
        }
    }
}
