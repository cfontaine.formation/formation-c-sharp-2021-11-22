﻿using System;

namespace _05_poo
{
    internal class Conteneur
    {
        private static string varClasse = "Variable de classe";

        private string varInstance = "Variable d'instance";

        public void Test()
        {
            Element elm = new Element();
            elm.TestVarClasse();
            elm.TestVarInstance(this);
        }

        // classe Imbriqué
        private class Element
        {
            public void TestVarClasse()
            {
                Console.WriteLine(varClasse); // on a accès aux variables de classe de la classe Conteneur
            }

            public void TestVarInstance(Conteneur c)
            {
                Console.WriteLine(c.varInstance);
            }
        }
    }
}
