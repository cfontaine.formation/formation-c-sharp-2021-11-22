﻿using System;

namespace _04_Méthodes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            double res = Multiplier(3.5, 6.7);
            Console.WriteLine(res);
            Console.WriteLine(Multiplier(3.5, 6.7));

            // Appel de methode (sans retour)
            Afficher(res);

            // Exercice maximum
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Maximum(a, b));

            // Exercice parité
            Console.WriteLine(Even(6));
            Console.WriteLine(Even(3));

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int c = 1;
            TestParamValeur(c);
            TestParamValeur(23);
            Console.WriteLine(c);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref c);
            Console.WriteLine(c);


            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée
            int o1;
            double o2;
            TestParamSortie(out o1, out o2);
            Console.WriteLine(o1 + " " + o2);

            // Déclarer la variable de retour dans les arguments pendant l'appel de la méthode
            TestParamSortie(out int d, out double val);
            Console.WriteLine(d + " " + val);

            // On peut ignorer un paramètre out en le nommant _
            TestParamSortie(out _, out double val2);
            Console.WriteLine(val2);

            // Paramètre optionnel
            TestParamOptionnel(1, "hello", true);
            TestParamOptionnel(23, "Bonjour");
            TestParamOptionnel(42);

            // Paramètres nommés
            TestParamOptionnel(i: 56, tst: true);
            TestParamOptionnel(tst: false, i: 5, str: "aaaa");

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(2));
            Console.WriteLine(Moyenne(1, 5, 4, 3, 7, 2, 8, 3, 5));
            int[] tab = { 1, 4, 7, 8 };
            Console.WriteLine(Moyenne(1, tab));

            // Exercice Tableau
            int[] t1 = SaisirTab();
            AfficherTab(t1);
            CalculTab(t1, out int max, out int min, out double moy);
            Console.WriteLine($"min={min} max={max} moy={moy}");

            // Surcharge de méthode
            Console.WriteLine(Somme(1, 3));         // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(2.3, 4.6));
            Console.WriteLine(Somme(1, 4.6));
            Console.WriteLine(Somme("Hello ", "world"));
            Console.WriteLine(Somme(1, 3, 3));

            Console.WriteLine(Somme(1, 45L));   // convertion de 45 en double -> appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme('z', 3));   // convertion de 'z' en int -> appel de la méthode avec 2 entiers en paramètre

            // Console.WriteLine(Somme(2.5, 5.6M)); // erreur pas de conversion possible
            foreach (var s in args)
            {
                Console.WriteLine(s);
            }

            // Méthode récursive
            double f = Factorial(3);
            Console.WriteLine(f);
            Console.ReadKey();
        }

        static double Multiplier(double v1, double v2)
        {
            return v1 * v2;   //  L'instruction return
                              // - Interrompt l'exécution de la méthode
                              // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(double d)  // void => pas de valeur retournée
        {
            Console.WriteLine(d);
            // avec void => return; ou return peut être omis
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum

        static double Maximum(double a, double b)
        {
            /*       if (a > b)
                   {
                       return a;
                   }
                   else 
                   {
                       return b;
                   }*/
            return a > b ? a : b;
        }
        #endregion

        #region Exercice Pair
        // Écrire une méthode even qui prend un entier en paramètre
        // Elle retourne vrai, si il est paire

        static bool Even(int valeur)
        {
            /*            if(valeur % 2 == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }*/
            return valeur % 2 == 0;  // valeur & 1==0;
        }
        #endregion

        #region Passage de paramètre
        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 42;     // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence -> ref
        static void TestParamReference(ref int a)
        {
            Console.WriteLine(a);
            a = 42;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie -> out
        static void TestParamSortie(out int a, out double v2)
        {
            //int tmp = a;
            a = 42; // La méthode doit obligatoirement affecter une valeur aux paramètres out
            int tmp = a;
            v2 = 12.3;
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(int i, string str = "chaine par défaut", bool tst = false)
        {
            Console.WriteLine($"i={i}  str={str} tst={tst}");
        }

        // Nombre d'arguments variable -> params
        static double Moyenne(int val, params int[] v)
        {
            double somme = val;
            for (int i = 0; i < v.Length; i++)
            {
                somme += v[i];
            }
            return somme / (v.Length + 1);
        }
        #endregion

        #region Exercice Tableau
        // Écrire un méthode qui affiche un tableau d’entier
        // Écrire une méthode qui permet de saisir :
        //  - La taille du tableau
        //  - Les éléments du tableau
        // Écrire une méthode qui calcule :
        // - le minimum d’un tableau d’entier
        // - le maximum
        // - la moyenne
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (int v in tab)
            {
                Console.Write(v + " ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()    // ou static void SaisirTab(out int[] t)
        {
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t = new int[size];

            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }
            return t;
        }

        static void CalculTab(int[] t, out int max, out int min, out double moy)
        {
            max = t[0];
            min = t[0];
            double somme = 0.0;
            foreach (var v in t)
            {
                if (v > max)
                {
                    max = v;
                }
                if (v < min)
                {
                    min = v;
                }
                somme += v;
            }
            moy = somme / t.Length;
        }
        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée -> plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static double Somme(double v1, double v2)
        {
            Console.WriteLine("2 doubles");
            return v1 + v2;
        }
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(int a, double b)
        {
            Console.WriteLine("un entier un double");
            return a + b;
        }

        static string Somme(string s1, string s2)
        {
            Console.WriteLine("2 chaines");
            return s1 + s2;
        }
        static int Somme(int v1, int v2, int v3)
        {
            Console.WriteLine("3 entiers");
            return v1 + v2 + v3;
        }
        #endregion

        #region Méthode récursive
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion
    }
}
