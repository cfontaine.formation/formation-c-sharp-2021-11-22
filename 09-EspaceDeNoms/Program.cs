﻿using _09_EspaceDeNoms.dao;
using _09_EspaceDeNoms.gui;
using ConsoleGui = _09_EspaceDeNoms.gui.Console;

// ALias
using ConsoleSys = System.Console;


namespace _09_EspaceDeNoms
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // _09_EspaceDeNoms.gui.Window win = new _09_EspaceDeNoms.gui.Window();
            Window win = new Window();
            //  System.Console.WriteLine();
            //  _09_EspaceDeNoms.gui.Console cnx = new _09_EspaceDeNoms.gui.Console();

            // avec un Alias
            ConsoleGui cnx = new ConsoleGui();
            ConsoleSys.WriteLine();

            UserDao uDao = new UserDao();
        }
    }
}
