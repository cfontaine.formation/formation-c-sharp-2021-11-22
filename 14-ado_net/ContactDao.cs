﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace _14_ado_net
{
    public class ContactDao
    {
        public static string ChaineConnection { get; set; }

        static MySqlConnection cnx;


        // Méthode pour persiter un objet ou le mettre à jour s'il existe dans la base de donnée
        // close à false permet de ne pas fermer la connection à la base de donnée
        public void SaveOrUpdate(Contact contact, bool close = true)
        {
            if (contact.Id == 0)
            {
                Create(GetConnection(), contact);
            }
            else
            {
                Update(GetConnection(), contact);
            }
            CloseConnection(close);
        }

        // Méthode pour supprimer un contact de la base de données
        public void Delete(Contact contact, bool close = true)
        {
            string req = @"DELETE FROM contacts WHERE id=@id";  // @ pour supprimer l'interprétation 
            MySqlCommand cmd = new MySqlCommand(req, GetConnection());
            cmd.Parameters.AddWithValue("@id", contact.Id);
            cmd.ExecuteNonQuery();
            CloseConnection(close);
        }

        // Méthode pour récupérer un contact dans la base de donnée en fonction de son id
        public Contact FindById(long id, bool close = true)
        {
            Contact contact = null;
            string req = @"SELECT id,prenom,nom,date_naissance,email FROM contacts  WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, GetConnection());
            cmd.Parameters.AddWithValue("@id", id);
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                contact = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("date_naissance"), reader.GetString("email"));
                contact.Id = reader.GetInt64("id");
            }
            CloseConnection(close);
            return contact;
        }

        // Méthode pour récupérer tous les contacts de la base de donnée
        public List<Contact> FindAll(bool close = true)
        {
            List<Contact> lst = new List<Contact>();
            string req = @"SELECT id,prenom,nom,date_naissance,email FROM contacts";
            MySqlCommand cmd = new MySqlCommand(req, GetConnection());
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Contact c = new Contact(reader.GetString("prenom"), reader.GetString("nom"), reader.GetDateTime("date_naissance"), reader.GetString("email"));
                c.Id = reader.GetInt64("id");
                lst.Add(c);
            }
            CloseConnection(close);
            return lst;
        }

        protected void Create(MySqlConnection cnx, Contact contact)
        {
            string req = "INSERT INTO contacts(prenom,nom,date_naissance,email)VALUES (@prenom,@nom,@dateNaissance,@email); ";
            MySqlCommand cmd = new MySqlCommand(req, GetConnection());
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.ExecuteNonQuery();
            contact.Id = cmd.LastInsertedId;
        }

        protected void Update(MySqlConnection cnx, Contact contact)
        {
            string req = "UPDATE contacts SET nom = @nom, prenom=@prenom, date_naissance=@dateNaissance, email=@email  WHERE id=@id";
            MySqlCommand cmd = new MySqlCommand(req, GetConnection());
            cmd.Parameters.AddWithValue("@id", contact.Id);
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@jdateNaissance", contact.DateNaissance);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.ExecuteNonQuery();
        }

        // Méthode qui permet d'obtenir la connection à la base de donnée
        private MySqlConnection GetConnection()
        {
            if (cnx == null)
            {
                cnx = new MySqlConnection(ChaineConnection);
                cnx.Open();
            }
            return cnx;
        }

        // Méthode qui permet de fermer la connection à la base de donnée
        private void CloseConnection(bool close)
        {
            if (cnx != null && close)
            {
                cnx.Close();
                cnx.Dispose();
                cnx = null;
            }
        }

    }
}
