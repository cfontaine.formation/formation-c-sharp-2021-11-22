﻿using System;
using System.Text;

namespace _01_base
{
    // Une enumération est un ensemble de constante
    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD = 90, EST = 0, SUD = 270, OUEST = 180 };
    enum Motorisation { DIESEL = 45, ESSENCE = 95, ELECTRIQUE = 2, GPL = 10, HYDROGENE };

    [Flags]
    enum Jour
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int X;
        public int Y;
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Variable et types simple
            // Déclaration d'une variable   type nomVariable;
            int i;
            // Console.WriteLine(i);    // Erreur -> on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration et initialisation de variable    type nomVariable = valeur;
            double d = 12.3;
            Console.WriteLine(d);

            // Déclaration multiple de variable
            int j, k = 4, l;
            Console.WriteLine(k);

            // Littéral booléan
            bool test = true; // false
            Console.WriteLine(test);

            // Littéral caractère
            char chr = 'a';
            char utf8Chr = '\u0045'; // Caractère en UTF-8
            char utf8ChrHexa = '\x45';
            Console.WriteLine(chr + " " + utf8Chr + " " + utf8ChrHexa); // + -> concaténation

            // Littéral entier -> int par défaut
            long l1 = 12L;      // L -> Littéral long
            uint ui = 1234U;    // U -> Littéral unsigned 
            Console.WriteLine(l1 + " " + ui);

            // Littéral entier -> chagement de base
            int dec = 100;          // décimal (base 10) par défaut
            int hexa = 0xF412;      // 0x -> héxadécimal (base 16)
            int bin = 0b10101010;   // 0b -> binaire
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante -> par défaut double 
            float f = 12.3F;        // F -> Littéral float
            decimal deci = 123.5M;  // M -> Littéral decimal
            Console.WriteLine(f + " " + deci);

            // Littéral nombre à virgule flottante
            double d2 = 100.4;
            double exp = 1e3;
            Console.WriteLine(d2 + " " + exp);

            // Type implicite -> var
            var v1 = 10;        // v1 -> double
            var v2 = 12.3F;     // v2 -> string

            // Avec @ on peut utiliser les mots réservés comme nom de variable (à éviter)
            int @if;
            @if = 10;
            Console.WriteLine(@if);
            #endregion

            #region Convertion
            // Transtypage implicite: ( pas de perte de donnée)
            // type inférieur => type supérieur
            int tii = 123;
            long til = tii;
            double tid = tii;
            Console.WriteLine(tii + " " + til + " " + tid);

            // Transtypage explicite: cast = (nouveauType)
            double ted = 12.5;
            int tei = (int)ted;
            Console.WriteLine(ted + " " + tei);
            decimal tedc = (decimal)ted;
            Console.WriteLine(ted + " " + tei + " " + tedc);

            // Fonction de convertion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            double fc1 = 42;
            int cnv1 = Convert.ToInt32(fc1);    // Convertion d'un double en entier
            char cnv2 = Convert.ToChar(cnv1);   // Convertion d'un entier en char
            Console.WriteLine(cnv1 + " " + cnv2);

            // On peut aussi convertir une chaine de caractères en un type numérique
            string str = "1234";
            int cnv3 = Convert.ToInt32(str);
            Console.WriteLine(cnv3);
            string res = Convert.ToString(cnv1);

            // Conversion d'une chaine de caractères en double
            // Parse
            Console.WriteLine(Double.Parse("1234,5"));
            //  Console.WriteLine(Int32.Parse("azerty"));   //  Erreur -> génère une excepti

            // TryParse
            double cnv5;
            bool tst = Double.TryParse("1234", out cnv5);   // Retourne true et la convertion est affecté à cnv5
            Console.WriteLine(cnv5 + " " + tst);
            tst = Double.TryParse("1234AAA", out cnv5);     // Erreur -> retourne false, 0 est affecté à cnv5
            Console.WriteLine(cnv5 + " " + tst);

            // Dépassement de capacité
            int dep = 300;            // 00000000 00000000 00000001 00101100    300
            sbyte sd = (sbyte)dep;    //                            00101100    44
            Console.WriteLine(dep + " " + sd);

            // checked / unchecked
            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral

            //checked
            //{
            //    dep = 300;
            //    sd = (sbyte)dep;
            //    Console.WriteLine(dep + " " + sd);  // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            unchecked // (par défaut )
            {
                sd = (sbyte)dep;
                Console.WriteLine(dep + " " + sd); // plus de vérification de dépassement de capacité
            }
            #endregion

            #region type reference
            StringBuilder s1 = new StringBuilder("hello");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);

            s2 = s1;    // s1 et s2 référence le même objet
            Console.WriteLine(s1 + " " + s2);
            s1 = null;

            Console.WriteLine(s1 + " " + s2);
            s2 = null;  // s1 et s2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            Console.WriteLine(s1 + " " + s2);
            #endregion

            #region type nullable
            // Nullable : permet d'utiliser null avec un type valeur
            double? tn = null;

            Console.WriteLine(tn.HasValue); // tn == null retourne false    
            tn = 1234;                      // Conversion implicite (int vers nullable)
            Console.WriteLine(tn.HasValue); // La propriété HasValue retourne true si  tn contient une valeur (!= null)

            double somme = tn.Value + 4.5;  // Pour récupérer la valeur, on peut utiliser la propriété Value
            Console.WriteLine(somme);
            Console.WriteLine((double)tn.Value);// ou, on peut faire un cast
            #endregion

            #region constante
            const double PI = 3.14;
            // PI = 3.1415; // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(PI * 2);
            #endregion

            #region Format de chaine de caractère
            int xi = 2;
            int yi = 3;
            Console.WriteLine("xi=" + xi + " " + "yi=" + yi);
            string resFormat = string.Format("xi={0} yi{1}", xi, yi);
            Console.WriteLine(resFormat);
            Console.WriteLine("xi={0} yi={1}", xi, yi); // on peut définir directement le format dans la méthode WriteLine

            Console.WriteLine($"xi={xi} yi={yi}");

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            Console.WriteLine("c:\temp\newfile.txt");
            // Chaînes textuelles => @ devant une littérale chaine de caractères
            //  => n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers) 
            Console.WriteLine(@"c:\temp\newfile.txt");
            #endregion


            #region Saisir une valeur au clavier
            //string valStr = Console.ReadLine();
            //int val = Convert.ToInt32(valStr);
            int val = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(val);
            #endregion

            #region Opérateur
            // Operateur arithméthique
            double op1 = 1.2;
            double op2 = 12.3;
            double opRes = op1 + op2;
            Console.WriteLine(opRes);
            int mod = 11 % 2;       // % => modulo (reste de division entière
            Console.WriteLine(mod);

            // Exercice: Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 = 4
            int e1 = Convert.ToInt32(Console.ReadLine());
            int e2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{e1} + {e2} = {e1 + e2}");

            // Exercice:Moyenne
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            int ne1 = Convert.ToInt32(Console.ReadLine());
            int ne2 = Convert.ToInt32(Console.ReadLine());
            double moyenne = ((double)(ne1 + ne2)) / 2;
            Console.WriteLine($"moyenne={moyenne}");

            // Exercice: Permutation de 2 variables 
            // Saisir 2 variables de type entier et les permuter avant de les afficher
            int pv1 = Convert.ToInt32(Console.ReadLine());
            int pv2 = Convert.ToInt32(Console.ReadLine());
            int tmp = v1;
            pv1 = pv2;
            pv2 = tmp;
            Console.WriteLine($" {pv1} {pv2}");

            // Opérateur d'incrémentation
            // Pré-incrémentation
            int inc = 0;
            int result = ++inc; // inc =1 result =1
            Console.WriteLine(inc + " " + result);

            // Post-incrémentation
            inc = 0;
            result = inc++;
            Console.WriteLine(inc + " " + result); // result=0 inc=1

            // Affectation composée
            inc += 4;   // équivaut à inc=inc+4:
            Console.WriteLine(inc);
            inc *= 2;  // équivaut à inc=inc*2:
            Console.WriteLine(inc);

            // Opérateur de comparaison
            bool tst1 = inc > 100;      // Une comparaison a pour résultat un booléen
            Console.WriteLine(tst1);    // false

            Console.WriteLine(inc == 5);    // true

            // Opérateur logique
            bool tst2 = !(inc > 100);   // ! => non
            Console.WriteLine(tst2);

            // et               ou
            // a b | a&&b       a b | a||b
            // V V | V          V V | V
            // V F | F          V F | V
            // F V | F          F V | V
            // F F | F          F F | F

            // Opérateur court-circuit && et ||
            // && => dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            int z = 10;
            Console.WriteLine(inc < 20 && z == 10);  // vrai
            Console.WriteLine(inc > 20 && z == 10);  // comme inc > 20 est faux, z == 10 n'est pas évalué

            // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            Console.WriteLine(inc == 5 || z == 10); // comme inc == 5  est vraie, z == 10 n'est pas évalué

            // Opérateur Binaires (bit à bit)
            byte b = 0b10011;
            Console.WriteLine(Convert.ToString(~b, 2));        // ~ -> complémént: 1 à 0 et 0 à 1
            Console.WriteLine(Convert.ToString(b & 0b101, 2)); // et -> 001
            Console.WriteLine(Convert.ToString(b | 0b101, 2)); // ou -> 10111
            Console.WriteLine(Convert.ToString(b ^ 0b101, 2)); // ou exclusif -> 10110
            Console.WriteLine(Convert.ToString(b << 2, 2));    // Décalage à gauche de 2 -> 1001100
            Console.WriteLine(Convert.ToString(b >> 1, 2));    // Décalage à droite de 1 -> 1001

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str2 = "azerty";
            string res1 = str2 ?? "default";
            Console.WriteLine(res1);
            str2 = null;
            res1 = str2 ?? "default";
            Console.WriteLine(res1);
            #endregion

            #region Promotion numérique
            int prn1 = 11;
            double prn2 = 45.6;
            // prn1 est promu en double -> Le type le + petit est promu vers le +grand type des deux
            double resPrn = prn1 + prn2;
            Console.WriteLine(resPrn);
            int resPrn2 = prn1 / 2;         // 5
            Console.WriteLine(resPrn2);
            double resPrn3 = prn1 / 2.0;    // 5.5 prn1 est promu en double
            Console.WriteLine(resPrn3);

            sbyte sb11 = 1;
            sbyte sb22 = 2; // sbyte, byte, short, ushort, char sont promus en int 
            int sbRes = sb11 + sb22;    // sb11b1 et sb22 sont promus en int
            Console.WriteLine(sbRes);
            #endregion
            #region Enumération
            // m est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation m = Motorisation.DIESEL;
            Console.WriteLine(m);

            // enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string strMr = m.ToString();
            Console.WriteLine(strMr);

            // enum ->  entier (cast)
            int iMr = (int)m;
            Console.WriteLine(iMr);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation mr = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL");
            Console.WriteLine(mr);
            // mr = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL2");    // si la chaine n'existe pas dans l'énumation => exception
            // Console.WriteLine(mr);

            // int -> enum
            iMr = 95;
            if (Enum.IsDefined(typeof(Motorisation), iMr))  // Permet de tester si la valeur entière existe dans l'enumération
            {
                mr = (Motorisation)iMr;
                Console.WriteLine(mr);
            }

            // Énumération comme indicateurs binaires
            Jour JourSemaine = Jour.LUNDI | Jour.MERCREDI; //001 | 100 =101
            if ((JourSemaine & Jour.LUNDI) != 0)     // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");         // 101 & 001 =1
            }
            Console.WriteLine(JourSemaine);         // teste la présence de MARDI
            if ((JourSemaine & Jour.MARDI) != 0)    // 101 & 010 =0
            {
                Console.WriteLine("Mardi");
            }
            JourSemaine |= Jour.SAMEDI;
            if ((JourSemaine & Jour.WEEKEND) != 0)
            {
                Console.WriteLine("Week-end");      // 0100101 & 1100000 = 0100000
            }

            switch (m)
            {
                case Motorisation.DIESEL:
                    Console.WriteLine("Diesel");
                    break;
                case Motorisation.ESSENCE:
                    Console.WriteLine("Essence");
                    break;
                default:
                    Console.WriteLine("Autre motorisation");
                    break;
            }
            #endregion
            #region Structure
            // Structure
            Point pA;
            pA.X = 10;  // accès au champs X de la structure
            pA.Y = 2;

            Console.WriteLine($"pA({pA.X}, {pA.Y})");
            Point pB = pA;
            Console.WriteLine($"pB({pB.X}, {pB.Y})");
            pB.X = -1;
            Console.WriteLine($"pA({pA.X}, {pA.Y})");
            Console.WriteLine($"pB({pB.X}, {pB.Y})");
            if (pA.X == pB.X && pA.Y == pB.Y)
            {
                Console.WriteLine("Les points égaux");
            }
            #endregion
            Console.ReadKey();
        }
    }
}
