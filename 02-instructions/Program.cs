﻿using System;

namespace _02_instructions
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Condition

            // Condition if
            int a = Convert.ToInt32(Console.ReadLine());
            if (a > 10)
            {
                Console.WriteLine("valeur supérieur à 10");
            }
            else if (a == 10)
            {
                Console.WriteLine("valeur  égale à 10");
            }
            else
            {
                Console.WriteLine("valeur inférieur 10");
            }

            // Exercice: Trie de 2 Valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            if (d1 < d2)
            {
                Console.WriteLine($"{d1} < {d2}");
            }
            else
            {
                Console.WriteLine($"{d2} < {d1}");
            }

            // Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle -4(exclu) et 7(inclu)
            int v = Convert.ToInt32(Console.ReadLine());
            if (v > -4 && v <= 7)
            {
                Console.WriteLine($"{v} fait parti de l'interval");
            }

            // Condition switch
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                //case 6:
                //case 7:
                case int jo when jo >= 6 && jo <= 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un nombre à virgule flottante v1
            // - une chaîne de caractère opérateur qui a pour valeur valide: + - * /
            // - un nombre à virgule flottante v2
            // Afficher:
            // - Le résultat de l’opération
            // - Un message d’erreur si l’opérateur est incorrect
            // - Un message d’erreur si l’on fait une division par 0
            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1}+{v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1}-{v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($"{v1}*{v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0) // (v2<0.00000000001 && v2>-0.00000000001)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{v1} /{v2} = {v1 / v2}");
                    }
                    break;
                default:
                    Console.WriteLine($"L'opérateur {op} est invalide");
                    break;
            }


            // Opérateur ternaire
            int va = Convert.ToInt32(Console.ReadLine());
            string msg = va > 20 ? "supérieur à 20" : "inférieur ou égal à 20";
            Console.WriteLine(msg);
            #endregion

            #region Boucle
            // Boucle while
            int j = 0;
            while (j < 10)
            {
                Console.WriteLine($"j={j}");
                j++;
            }

            // Boucle do while
            j = 0;
            do
            {
                Console.WriteLine($"j={j}");
                j++;
            }
            while (j < 10);

            // Boucle for
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"i={i}");
            }
            #endregion

            #region Instructions de branchement

            // break
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    break;  // break => termine la boucle
                }
                Console.WriteLine($"i={i}");
            }

            // continue
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine($"i={i}");
            }

            // goto
            for (int i = 0; i < 10; i++)
            {
                for (int k = 0; k < 10; k++)
                {
                    if (i == 3)
                    {
                        goto EXIT_LOOP; // Utilisation de goto pour se branche sur le label EXIT_LOOP et quitter les 2 boucles imbriquées
                    }
                    Console.WriteLine($"i={i} k={k}");
                }
            }

        EXIT_LOOP:
            // Utilisation de goto pour transférer le contrôle à un case ou à l’étiquette par défaut d’une instruction switch
            jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto default;//goto case 7;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion

            #region Exercice boucle
            // Exercice: Table de multiplication
            // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
            //
            // 1 X 4 = 4
            // 2 X 4 = 8
            //  …
            // 9 x 4 = 36
            //
            // Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur

            for (; ; ) // Boucle infinie
            {
                Console.Write("Entrer un nombre ");
                int num = Convert.ToInt32(Console.ReadLine());
                if (num < 1 || num > 9)
                {
                    break;
                }
                for (int i = 1; i <= 9; i++)
                {
                    Console.WriteLine($"{i} x {num} = {i * num }");
                }
            }

            // Exercice: Quadrillage 
            // un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  

            // ex: pour 2 3  
            // [ ][ ]  
            // [ ][ ]  
            // [ ][ ]
            Console.WriteLine("Entrer le nombre de colonne ");
            int col = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Entrer le nombre de ligne ");
            int row = Convert.ToInt32(Console.ReadLine());

            for (int l = 0; l < row; l++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ] ");
                }

                Console.WriteLine(""); // Console.Write("\n");
            }
            #endregion
            Console.ReadKey();
        }

    }
}
