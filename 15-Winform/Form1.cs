﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _15_Winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {

            InitializeComponent();
        }

        private void bp1_Click(object sender, EventArgs e)
        {
            DialogResult dr=MessageBox.Show("J'ai cliquer sur le bouton ok", "titre", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            if(f.ShowDialog(this)== DialogResult.OK)
            {
                MessageBox.Show("contact saisie");
            }
            f.Close();
            f.Dispose();
        }
    }
}
