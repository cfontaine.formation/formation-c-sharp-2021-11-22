﻿namespace _06_polymorphisme
{
    internal class Animalerie
    {
        public Animal[] Animaux { get; set; } = new Animal[20];

        public void Ecouter()
        {
            foreach (Animal a in Animaux)
            {
                if (a != null)
                {
                    a.EmettreSon();
                }
            }
        }
    }
}
