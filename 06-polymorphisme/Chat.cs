﻿using System;

namespace _06_polymorphisme
{
    internal class Chat : Animal, IPeutMarcher
    {
        public int NbVie { get; set; } = 9;

        public Chat(int age, int poid, int nbVie) : base(age, poid)
        {
            NbVie = nbVie;
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Le chat miaule");
        }

        public void Marcher()
        {
            Console.WriteLine($"Le chat marche");
        }

        public void Courir()
        {
            Console.WriteLine($"Le chiat court");
        }
    }
}
