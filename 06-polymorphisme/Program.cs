﻿using System;
namespace _06_polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Animal a = new Animal(3, 3000);    // Impossible la classe est abstraite
            //a.EmettreSon();

            Chien ch1 = new Chien(4, 4000, "Idefix");   //  On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                        // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
            ch1.EmettreSon();                           // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée

            Animal a2 = new Chien(8, 5000, "Laika");
            a2.EmettreSon();

            //if (a1 is Chien)           // test si a2 est de "type" Chien
            //{
            // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
            //    Chien c2 =(Chien) a2;   
            //    Chien ch2 = a1 as Chien;       // as equivalant à un cast pour un objet
            //    Console.WriteLine(ch2.Nom);    // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
            //}

            if (a2 is Chien ch2)    // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                // Chien ch2 = (Chien)a;
                // Chien ch2 = a2 as Chien;
                ch2.EmettreSon();
            }

            Animalerie an = new Animalerie();
            an.Animaux[0] = new Chien(4, 4000, "Idefix");
            an.Animaux[1] = new Chien(8, 5000, "Laika");
            an.Animaux[2] = new Chat(4, 2500, 6);
            an.Animaux[3] = new Chien(5, 7000, "Rolo");
            an.Ecouter();

            // Object
            // ToString()
            // Console.WriteLine(ch1.ToString());
            Console.WriteLine(ch1);
            // Equals
            Chien ch3 = new Chien(4, 4000, "Idefix");
            Console.WriteLine(ch1 == ch3); // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents -> false
            Console.WriteLine(ch1.Equals(ch3)); // -> true

            // Interface
            IPeutMarcher[] tab = new IPeutMarcher[5]; // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            tab[0] = new Chien(4, 4000, "Idefix");
            tab[1] = new Chien(8, 5000, "Laika");
            tab[2] = new Chat(4, 2500, 6);
            tab[3] = new Canard(8, 5000);
            tab[4] = new Chat(2, 3500, 7);

            foreach(var v in tab)
            {
                v.Courir();

            }
            Console.ReadKey();
        }
    }
}
