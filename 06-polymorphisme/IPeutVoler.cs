﻿namespace _06_polymorphisme
{
    internal interface IPeutVoler
    {
        void decoller();

        void atterrir();
    }
}
