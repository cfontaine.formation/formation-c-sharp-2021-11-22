﻿using System;

namespace _06_polymorphisme
{
    internal class Canard : Animal, IPeutMarcher, IPeutVoler     // On peut implémenter plusieurs d'interface
    {
        public Canard(int age, int poid) : base(age, poid)
        {
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Coin coin");
        }

        public void Marcher()
        {
            Console.WriteLine($"Le canard Marche");
        }

        public void Courir()
        {
            Console.WriteLine($"Le canard Court");
        }

        public void decoller()
        {
            Console.WriteLine($"Le canard décole");
        }

        public void atterrir()
        {
            Console.WriteLine($"Le canard atterie");
        }
    }
}
