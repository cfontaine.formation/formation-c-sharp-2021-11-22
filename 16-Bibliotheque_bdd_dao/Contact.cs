﻿using System;

namespace _16_Bibliotheque_bdd_dao
{
    public class Contact : DbObject
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        public DateTime DateNaissance { get; set; }

        public string Email { get; set; }

        public Contact(string prenom, string nom, DateTime dateNaissance, string email)
        {
            Prenom = prenom;
            Nom = nom;
            DateNaissance = dateNaissance;
            Email = email;
        }

        public override string ToString()
        {
            return string.Format($"{Prenom} {Nom} {DateNaissance} {Email} ({Id})");
        }
    }

}
