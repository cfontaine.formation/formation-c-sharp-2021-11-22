﻿namespace _07_Exercice_Polymorphisme
{
    class TriangleRectangle : Rectangle
    {
        public TriangleRectangle(Couleur couleur, double longueur, double largeur) : base(couleur, longueur, largeur)
        {
        }

        public override double CalculSurface()
        {
            return base.CalculSurface() / 2.0;
        }
    }

}
