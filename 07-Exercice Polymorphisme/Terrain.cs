﻿namespace _07_Exercice_Polymorphisme
{
    class Terrain
    {
        private Forme[] _formes;
        public int NbForme { get; set; }

        public Terrain() : this(10)
        {

        }

        public Terrain(int size)
        {
            _formes = new Forme[size];
        }

        public void Ajouter(Forme f)
        {
            if (NbForme < _formes.Length)
            {
                _formes[NbForme] = f;
                NbForme++;
            }
        }

        public double Surface()
        {
            double somme = 0;
            for (int i = 0; i < NbForme; i++)
            {
                somme += _formes[i].CalculSurface();
            }
            return somme;
        }

        public double Surface(Couleur c)
        {
            double somme = 0;
            for (int i = 0; i < NbForme; i++)
            {
                if (_formes[i].CouleurF == c)
                {
                    somme += _formes[i].CalculSurface();
                }
            }
            return somme;
        }
    }

}
