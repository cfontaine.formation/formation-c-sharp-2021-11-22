﻿namespace _07_Exercice_Polymorphisme
{
    class Rectangle : Forme
    {
        double Longueur { get; set; }

        double Largeur { get; set; }

        public Rectangle(Couleur couleur, double longueur, double largeur) : base(couleur)
        {
            Longueur = longueur;
            Largeur = largeur;
        }

        public override double CalculSurface()
        {
            return Longueur * Largeur;
        }
    }

}
