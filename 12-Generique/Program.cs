﻿using System;

namespace _12_Generique
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Classe générique
            Box<double> bo1 = new Box<double>(3.4, 5.6);
            bo1.Afficher();
            Console.WriteLine(bo1.A);
            Box<string> bo2 = new Box<string>("azerty", "sdfghh");
            bo2.Afficher();

            // Méthode générique
            //TestMethodGenerique.MethodeGenerique<string>("test");
            TestMethodGenerique.MethodeGenerique("test");
            // TestMethodGenerique.MethodeGenerique<int>(1);
            TestMethodGenerique.MethodeGenerique(1);
            Console.ReadKey();
        }
    }
}
