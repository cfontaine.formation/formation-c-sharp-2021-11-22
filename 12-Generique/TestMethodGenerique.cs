﻿using System;

namespace _12_Generique
{
    class TestMethodGenerique
    {
        public static void MethodeGenerique<T>(T a)
        {
            Console.WriteLine(a);
        }
    }
}
